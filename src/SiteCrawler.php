<?php

namespace SiteCrawler;

//use SiteCrawler\Classes\HtmlDomParser;
use SiteCrawler\Classes\SiteCrawlerParser;
use SiteCrawler\Classes\SiteCrawlerClient;

/**
 * @property mixed|string domain
 * @property array viewedLinks
 * @property array linksToParse
 */
class SiteCrawler
{
    private array $linksToParse;
    private array $viewedLinks;
    private array $result;
    private array $errors;
    private string $baseUrl;
    private SiteCrawlerClient $client;
    //private HtmlDomParser $parser;
    private SiteCrawlerParser $parser;

    //const TAG_PATTERNS = [
    //    ['name' => 'title', 'selector' => 'title', 'needed' => 'plaintext'],
    //    ['name' => 'h1', 'selector' => 'h1', 'needed' => 'plaintext'],
    //    ['name' => 'description', 'selector' => 'meta[name=description]', 'needed' => 'content'],
    //    ['name' => 'a', 'selector' => 'a',  'needed' => 'href'],
    //];

    const TAG_PATTERNS = [
        ['name' => 'title', 'selector' => 'title'],
        ['name' => 'h1', 'selector' => 'h1'],
        ['name' => 'description', 'selector' => 'name="description"'],
        ['name' => 'a', 'selector' => 'a'],
    ];

    /**
     * SiteCrawler constructor.
     * @param string $domain
     */
    public function __construct(string $domain)
    {
        $this->num = 0;
        $this->linksToParse = array();
        $this->viewedLinks = array();
        $this->result = array();
        $this->errors = array();
        $this->baseUrl = $this->formattedDomain($domain);
        $this->client = new SiteCrawlerClient($domain);
        //$this->parser = new HtmlDomParser($domain);
        $this->parser = new SiteCrawlerParser($domain);
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->baseUrl;
    }

    /**
     * @param string $domain
     * @return int
     */
    private function getStatusCode(string $domain): int
    {
        $request = $this->client->getStatusCode($domain);

        if ($request['code'] == 404) {
            $this->errors[] = $request['error'];
        }

        if ($domain !== $this->baseUrl.'/') {
            $this->result[$domain]['statusCode'] = $request['code'];
            $this->result[$domain]['title'] = [];
            $this->result[$domain]['h1'] = [];
            $this->result[$domain]['description'] = [];
        }

        return $request['code'];
    }

    /**
     * @param string $domain
     * @return bool
     */
    private function isDomainAvailable(string $domain) :bool
    {
        if (!filter_var($domain, FILTER_VALIDATE_URL)){
            return false;
        }

        $code = $this->getStatusCode($domain);

        if ($code == 200) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $domain
     * @return string
     */
    public function formattedDomain(string $domain): string
    {
        if (!str_contains($domain, 'http')) {
            $domain = 'https://' . $domain;
        }

        if (substr($domain, -1) == '/') {
            $domain = substr($domain, 0, -1);
        }

        return $domain;
    }

    /**
     * @return bool|array
     */
    private function parseLinksArray(): bool|array
    {
        $toQueue = [];
        $this->viewedLinks = array_merge($this->viewedLinks, $this->linksToParse);

        foreach ($this->linksToParse as $item) {
            $html = $this->parser->loadHtml($item);

            if (!is_null($html)) {
                $data = $this->getMetaData($html);
                $result[$item] = array_merge($this->result[$item], $data['result']);

                foreach ($data['links'] as $link) {
                    if ($this->checkLink($link, $toQueue)) {
                        $toQueue[] = $link;
                    }
                }
                $toQueue = array_unique($toQueue);
            }

            unset($data);
            unset($html);
        }

        $this->linksToParse = [];
        $this->linksToParse = $toQueue;

        if (isset($result)) {
            return $result;
        }

        return false;
    }

    /**
     * @param string $link
     * @param array $links
     * @return bool
     */
    private function checkLink(string $link, array $links) :bool
    {
        $bool = false;

        if ($link
            && !in_array($link, $links)
            && $link != $this->baseUrl
            && $link != $this->baseUrl.'/'
            && !in_array($link, $this->viewedLinks)
            && !str_contains($link, 'mailto:')
            && !str_contains($link, 'tel:')
            && !str_contains($link, 'javascript:')
            && !str_contains($link, '#')
            && !str_contains($link, '/feed')
            && !str_contains($link, 'wp-admin')
            && !str_contains($link, '.mp4')
            && !str_contains($link, '.js')
            && !str_contains($link, '.ico')
            && !str_contains($link, '.pdf')
            && !str_contains($link, '.png')
            && !str_contains($link, '.jpg')
            && !str_contains($link, 'xml')
            && !str_contains($link, 'wp-json')) {
            $bool = true;
        }

        return $bool;
    }

    /**
     * @param $html
     * @return array
     */
    private function getMetaData($html) :array
    {
        $result = [];
        $_links = [];

        foreach (self::TAG_PATTERNS as $tag) {
            $tagElement = $this->parser->findTagElement($html, $tag['selector']);
            //$needed = $tag['needed'];
            //при использовании библиотеки simplehtmldom вместо $item использовать $item->$needed

            if ($tag['name'] !== 'a') {
                if (!empty($tagElement)) {
                    foreach ($tagElement as $item) {
                        $result[$tag['name']][] = trim($item);
                    }
                } else {
                    $result[$tag['name']] = [];
                }
            } else {
                if (!empty($tagElement)) {
                    foreach ($tagElement as $item) {
                        if ($this->checkLink($item, $_links)) {
                            if (str_contains($item, $this->baseUrl)
                                && $this->isDomainAvailable($item)
                            ) {
                                $_links[] = $item;
                            } elseif (!str_contains($item, 'http')) {

                                if (substr($item, 0, 1) != '/') {
                                    $itemLink = $this->baseUrl.'/'.$item;
                                } else {
                                    $itemLink = $this->baseUrl.$item;
                                }

                                if ($this->isDomainAvailable($itemLink)
                                    && $item != '/') {
                                    $_links[] = $itemLink;
                                }
                            }
                        }
                    }
                }
            }
        }

        $_links = array_unique($_links);

        return ['result' => $result,'links' => $_links];
    }

    public function parse(): array
    {
        $start = microtime( true );

        $resArray = [];

        if ($this->isDomainAvailable($this->baseUrl)) {
            $html = $this->parser->loadHtml($this->baseUrl);
            $data = $this->getMetaData($html);
            $this->linksToParse = $data['links'];
            $this->result[$this->baseUrl] = array_merge($this->result[$this->baseUrl], $data['result']);

            unset($data);
            unset($html);

            $this->checkLinks();
        }

        $diff = sprintf( '%.6f sec.', microtime( true ) - $start );

        $resArray['pages'] = $this->result;
        $resArray['errors'] = $this->errors;
        $resArray['time'] = "Время выполнения: $diff";

        return $resArray;
    }

    private function checkLinks(): void
    {

        if (!empty($this->linksToParse)) {
            $resultLinksArray = $this->parseLinksArray();
            $this->result = array_merge($this->result, $resultLinksArray);

            $this->checkLinks();
        }
    }

}