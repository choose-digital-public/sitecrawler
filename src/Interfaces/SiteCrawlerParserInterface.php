<?php

namespace SiteCrawler\Interfaces;

interface SiteCrawlerParserInterface
{
    public function loadHtml(string $url);
    public function findTagElement(string $html, string $selector);
}