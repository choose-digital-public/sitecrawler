<?php

namespace SiteCrawler\Classes;

use SiteCrawler\Interfaces\SiteCrawlerParserInterface;

class SiteCrawlerParser implements SiteCrawlerParserInterface
{
    private string $baseUrl;

    /**
     * Classes constructor.
     * @param string $baseUrl
     */
    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $url
     * @return bool|string
     */
    public function loadHtml(string $url): bool|string
    {
        $html = file_get_contents($url);

        if ($http_response_header[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            return $html;
        }
    }

    /**
     * @param string $html
     * @param string $selector
     * @return mixed
     */
    public function findTagElement (string $html, string $selector): array
    {
        if ($selector == 'title') {
            preg_match_all('/\<'.$selector.'.*\>(?P<text>[^\<]+)\<\/'.$selector.'\>/', $html, $array);
            $result = $array['text'];
        } elseif ($selector == 'h1') {
            preg_match_all('/\<'.$selector.'.*\>(?P<text>[^\<]+)\<\/'.$selector.'\>/', $html, $array);
            $result = $array['text'];
        } elseif ($selector == 'name="description"') {
            preg_match_all('#'.$selector.'+\s+content="(.*?)"#ui', $html, $array);
            $result = $array[1];
        } else {
            preg_match_all('<a\s.*?href="(.+?)".*?>', $html, $array);
            $result = $array[1];
        }

        return $result;
    }
}