<h1>Библиотека для парсинга метатегов сайта</h1>

<h2>Для установки выполнить команду:</h2>

composer require sitecrawler/sitecrawler

<h2>Требования</h2>

Необходимо задать следующие значения php: <br>
set_time_limit(-1); <br>
ini_set('memory_limit', -1); <br>
ini_set('max_execution_time', -1);

<h2>Использование</h2>

Подключение: <br>
use SiteCrawler\Parser;

Запуск парсера: <br>
$parser = new Parser("Домен"); <br>
$result = $parser->parse();

Вернуть домен: <br>
$domain = $parser->getDomain();

Возвращает массив всех страниц сайта с метатегами, а также ошибки, если таковые имеются.  <br> <br>
Метатеги:
- h1
- title
- description